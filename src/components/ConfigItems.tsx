import React, {
    type JSX,
    type ChangeEvent,
    type Dispatch,
    type SetStateAction
} from 'react'
import {
    Checkbox,
    Accordion,
    Paragraph,
    Note,
    List,
} from '@contentful/f36-components'
import { Image } from '@contentful/f36-image'
import { CodeBlock } from '../components/CodeBlock'
import { codeBlockFieldPropsMin } from '../util/createCodeBlockType'

import fieldSetExampleImage from '../assets/field-set-example.png'

import { type AppInstallationParameters, type ConfigAccordionState } from '../locations/ConfigScreen'

interface ConfigAccordionBaseProps {
    accordionState: ConfigAccordionState,
    setAccordionState: Dispatch<SetStateAction<ConfigAccordionState>>
}

interface AutoConfigItemProps extends ConfigAccordionBaseProps {
    appInstallationParameters: AppInstallationParameters,
    onChange: (e?: ChangeEvent<HTMLInputElement>) => void,
}

const AutoConfigItem = ({
    appInstallationParameters,
    onChange,
    accordionState,
    setAccordionState
}: AutoConfigItemProps
): JSX.Element => {
    const {
        isContentTypeSet,
        userCheckAutoInstall,
    } = appInstallationParameters

    const isExpanded = accordionState.auto

    const handleExpand = () => {
        setAccordionState({auto: true, manual: false})
    }
    const handleCollapse = () => {
        setAccordionState((state) => {
            return {...state, auto: false}
        })
    }

    return (
        <Accordion.Item
            title='Automatic Configuration'
            isExpanded={isExpanded}
            onExpand={handleExpand}
            onCollapse={handleCollapse}
        >
            {
                isContentTypeSet
                    ? <Note variant="positive">Complete</Note>
                    : <Checkbox
                        name="install-code-block-type"
                        id="install-code-block-type"
                        isChecked={userCheckAutoInstall}
                        onChange={onChange}
                        isDisabled={isContentTypeSet}
                    >
                        <span>
                        Install the <code>Code Block</code> content type.
                        </span>
                    </Checkbox>
            }
        </Accordion.Item>
    )
}

type ManualConfigItemProps = ConfigAccordionBaseProps

const ManualConfigItem = ({
    accordionState,
    setAccordionState
}: ManualConfigItemProps
): JSX.Element => {
    const isExpanded = accordionState.manual

    const handleExpand = () => {
        setAccordionState({auto: false, manual: true})
    }
    const handleCollapse = () => {
        setAccordionState((state) => {
            return {...state, manual: false}
        })
    }
    return (
        <Accordion.Item
            title="Manual Configuration"
            isExpanded={isExpanded}
            onExpand={handleExpand}
            onCollapse={handleCollapse}
        >
            <Paragraph>
            Once the app is installed, create a content type
            (or update an exiting one) to contain the following fields:
                <List>
                    <List.Item>
                        <span><code>style</code> (Short text)</span>
                    </List.Item>
                    <List.Item>
                        <span><code>code</code> (Long text)</span>
                    </List.Item>
                </List>
                <CodeBlock code={JSON.stringify(codeBlockFieldPropsMin, null, 2)} language='json' />
            </Paragraph>
            <Paragraph>
                Then set the appearance of the <code>code</code> field
                to use the custom editor.
                <Image
                    height="281px"
                    width="500px"
                    src={fieldSetExampleImage}
                    alt='Example of setting the `code` field editor.'
                />
            </Paragraph>
        </Accordion.Item>
    )
}

const ConfigItems = { Manual: ManualConfigItem, Auto: AutoConfigItem }

export default ConfigItems