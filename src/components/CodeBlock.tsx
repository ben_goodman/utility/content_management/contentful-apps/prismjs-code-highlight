import React, { type JSX, useEffect } from 'react'

const PRISMJS_VERSION = '1.29.0'

const loadScript = (src: string) => {
    return new Promise<void>((resolve, reject) => {
        if (typeof window !== 'undefined') {
            const script = window.document.createElement('script')
            script.src = src
            script.addEventListener('load', function () {
                resolve()
            })
            script.addEventListener('error', function (e) {
                reject(e)
            })
            window.document.body.appendChild(script)
        }
    })
}

const loadStyle = (href: string) => {
    return new Promise<void>((resolve, reject) => {
        if (typeof window !== 'undefined') {
            const link = window.document.createElement('link')
            link.rel = 'stylesheet'
            link.href = href
            link.addEventListener('load', function () {
                resolve()
            })
            link.addEventListener('error', function (e) {
                reject(e)
            })
            window.document.body.appendChild(link)
        }
    })
}

export type CodeSnippetProps = {
  language: string;
  code: string;
}

export const CodeBlock = ({
    language,
    code,
}: CodeSnippetProps): JSX.Element => {

    useEffect(() => {
        const loadPrism = async () => {
            if (!window.Prism) {
                await loadStyle(`https://cdnjs.cloudflare.com/ajax/libs/prism/${PRISMJS_VERSION}/themes/prism.min.css`)
                await loadScript(`https://cdnjs.cloudflare.com/ajax/libs/prism/${PRISMJS_VERSION}/components/prism-core.min.js`)
                await loadScript(`https://cdnjs.cloudflare.com/ajax/libs/prism/${PRISMJS_VERSION}/plugins/autoloader/prism-autoloader.min.js`)
            }
        }
        loadPrism().then(() => {
            window.Prism.highlightAll()
        })
    }, [language])

    return (
        <pre>
            <code className={`language-${language}`}>
                {code}
            </code>
        </pre>
    )
}