import { type ConfigAppSDK } from '@contentful/app-sdk'
import { type CreateContentTypeProps, type Control } from 'contentful-management'

export const codeBlockFieldPropsMin = [
    {

        id: 'style',
        name: 'Style',
        type: 'Symbol',
        localized: false,
        required: true,
        validations: [
            {
                in: [
                    'bash',
                    'dockerfile',
                    'makefile',
                    'hcl',
                    'js',
                    'json',
                    'jsx',
                    'rust',
                    'text',
                    'tsx',
                    'ts',
                    'yaml',
                    'yml',
                    'html',
                    'css',
                    'mermaid',
                    'markdown',
                    'go'
                ],
                message: ''
            }
        ],
        disabled: false,
        omitted: false
    },
    {
        id: 'code',
        name: 'Code',
        type: 'Text',
        localized: false,
        required: true,
        validations: [],
        disabled: false,
        omitted: false
    }
]

export const codeBlockFieldProps: CreateContentTypeProps = {
    name: 'Code Block',
    description: 'A syntax-highlighted text field with optional fields for a description and caption.',
    displayField: 'title',
    fields: [
        {
            id: 'title',
            name: 'Title',
            type: 'Symbol',
            localized: false,
            required: false,
            validations: [
                {
                    unique: true
                }
            ],
            disabled: false,
            omitted: false
        },
        ...codeBlockFieldPropsMin,
        {
            id: 'description',
            name: 'Description',
            type: 'Symbol',
            localized: false,
            required: true,
            validations: [],
            disabled: false,
            omitted: false
        },
        {
            id: 'caption',
            name: 'Caption',
            type: 'Text',
            localized: false,
            required: false,
            validations: [],
            disabled: false,
            omitted: false
        }
    ]
}

export const createCodeBlockType = async (
    contentTypeId: string,
    sdk: ConfigAppSDK
): Promise<void> => {
    const cma = sdk.cma
    // create the 'CodeBlock' content type.
    const contentTypeProps = await cma.contentType.createWithId(
        { contentTypeId },
        codeBlockFieldProps
    )

    await cma.contentType.publish({ contentTypeId }, contentTypeProps )

    const fieldControls: Control[] = [
        {
            fieldId: 'code',
            widgetId: sdk.ids.app,
            widgetNamespace: 'app',
        }
    ]
    // assign the `prismjs-code-highlight` app as the 'code' field editor appearance.
    const editorInterface = await cma.editorInterface.get({contentTypeId})
    editorInterface.controls = fieldControls
    await cma.editorInterface.update({contentTypeId}, editorInterface)
}

export const hasCodeBlockType = async (
    contentTypeId: string,
    sdk: ConfigAppSDK
): Promise<boolean> => {
    const cma = sdk.cma
    try {
        await cma.contentType.get({contentTypeId})
        return true
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
        if (e.code === 'NotFound') {
            return false
        }
        throw e
    }
}