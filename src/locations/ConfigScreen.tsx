import React, { useCallback, useState, useEffect } from 'react'
import { css } from 'emotion'
import { type ConfigAppSDK } from '@contentful/app-sdk'
import {
    Heading,
    Form,
    Paragraph,
    Flex,
    Accordion,
} from '@contentful/f36-components'
import { useSDK } from '@contentful/react-apps-toolkit'
import { createCodeBlockType, hasCodeBlockType } from '../util/createCodeBlockType'
import ConfigItems from '../components/ConfigItems'

const CONTENT_TYPE_ID = 'code-block-v1'

export interface AppInstallationParameters {
    isContentTypeSet: boolean
    userCheckAutoInstall: boolean
}

export interface ConfigAccordionState {
    auto: boolean
    manual: boolean
}

const DEFAULT_APP_PARAMS: AppInstallationParameters = {
    isContentTypeSet: true,
    userCheckAutoInstall: false,
}

const DEFAULT_CONFIG_ACCORDION_STATE: ConfigAccordionState = {
    auto: true,
    manual: false
}

const ConfigScreen = () => {
    const [parameters, setParameters] = useState<AppInstallationParameters>(
        DEFAULT_APP_PARAMS
    )

    const [accordionState, setAccordionState]
        = useState(DEFAULT_CONFIG_ACCORDION_STATE)

    const sdk = useSDK<ConfigAppSDK>()

    // check if the 'CodeBlock' content type exists.
    useEffect(() => {
        (async () => {
            try {
                const isContentTypeSet =
                    await hasCodeBlockType(CONTENT_TYPE_ID, sdk)
                setParameters(s => ({...s, isContentTypeSet}))
            } catch (e) {
                console.error(e)
            }
        })()
    }, [])

    // handle checkbox for auto-installation of the 'CodeBlock' content type.
    const handleCheckboxChange = (
        e?: React.ChangeEvent<HTMLInputElement>
    ): void => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const { checked } = e!.target
        if (!checked) {
            setAccordionState({auto: false, manual: true})
        }
        setParameters(s => ({...s, userCheckAutoInstall: checked}))
    }

    const onConfigure = useCallback(async () => {
    // This method will be called when a user clicks on "Install"
    // or "Save" in the configuration screen.

        // Create the content type if it doesn't exist and the user has requested it.
        if (!parameters.isContentTypeSet && parameters.userCheckAutoInstall) {
            await createCodeBlockType(CONTENT_TYPE_ID, sdk)
            await hasCodeBlockType(CONTENT_TYPE_ID, sdk)
                .then((isContentTypeSet) => {
                    setParameters(s => ({...s, isContentTypeSet}))
                })
        }

        // Get current the state of EditorInterface and other entities
        // related to this app installation
        const currentState = await sdk.app.getCurrentState()

        return {
            // Parameters to be persisted as the app configuration.
            parameters,
            // In case you don't want to submit any update to app
            // locations, you can just pass the currentState as is
            targetState: currentState,
        }
    }, [parameters, sdk])

    useEffect(() => {
    // `onConfigure` allows to configure a callback to be
    // invoked when a user attempts to install the app or update
    // its configuration.
        sdk.app.onConfigure(() => onConfigure())
    }, [sdk, onConfigure])

    useEffect(() => {
        (async () => {
            // Get current parameters of the app.
            // If the app is not installed yet, `parameters` will be `null`.
            const currentParameters: AppInstallationParameters | null =
                await sdk.app.getParameters()

            if (currentParameters) {
                setParameters(currentParameters)
            }

            // Once preparation has finished, call `setReady` to hide
            // the loading screen and present the app to a user.
            sdk.app.setReady()
        })()
    }, [sdk])

    return (
        <Flex flexDirection="column" className={css({ margin: '80px', maxWidth: '800px' })}>
            <Form>
                <Heading>Code Syntax Highlighter Config</Heading>
                <Paragraph>
                    <span>This application provides a custom text field editor
                    with a syntax-highlighted preview pane.
                    Optionally, it provides a default content
                    type <code>Code Block</code> which already implements
                    the custom editor. You may choose to either install the
                    application&apos;s default <code>Code Block</code> content
                    type automatically or manually set it within an
                    existing content type.</span>
                </Paragraph>

                <Accordion>
                    <ConfigItems.Auto
                        appInstallationParameters={parameters}
                        onChange={handleCheckboxChange}
                        accordionState={accordionState}
                        setAccordionState={setAccordionState}
                    />
                    <ConfigItems.Manual
                        accordionState={accordionState}
                        setAccordionState={setAccordionState}
                    />
                </Accordion>

            </Form>
        </Flex>
    )
}

export default ConfigScreen
