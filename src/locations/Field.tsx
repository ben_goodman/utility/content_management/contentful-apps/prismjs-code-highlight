import React, { useEffect, useState } from 'react'
import { Tabs, FormControl, Box } from '@contentful/f36-components'
import { type FieldAppSDK, type EntryAPI } from '@contentful/app-sdk'

import { MultipleLineEditor } from '@contentful/field-editor-multiple-line'

import { useSDK } from '@contentful/react-apps-toolkit'
import { CodeBlock } from '../components/CodeBlock'
import styled from 'styled-components'

const StyledMultilineEditor = styled.div`
    textarea {
        min-height: 300px;
        margin-top: 3px;
    }
`

const Field = () => {
    const sdk = useSDK<FieldAppSDK>()
    const entry = sdk.entry as EntryAPI
    const { style, code } = entry.fields

    const [codeInputValue, setCodeInputValue] = useState(code.getValue())
    const [codeStyleValue, setCodeStyleValue] = useState(style.getValue())

    useEffect(() => {
        sdk.window.startAutoResizer()
        return sdk.window.stopAutoResizer
    }, [sdk])

    useEffect( () => {
        const detachChangeHandler = style.onValueChanged((value) => {
            if (value !== codeStyleValue) {
                setCodeStyleValue(value)
            }
        })
        return detachChangeHandler
    }, [style, codeStyleValue])

    useEffect( () => {
        const detachChangeHandler = code.onValueChanged((value) => {
            if (value !== codeInputValue) {
                setCodeInputValue(value)
            }
        })
        return detachChangeHandler
    }, [code, codeInputValue])

    return (
        <Box style={{minHeight: '300px', marginRight: '5px'}}>
            <Tabs
                defaultTab="preview"
            >

                <Tabs.List variant="horizontal-divider">
                    <Tabs.Tab panelId="editor">Editor</Tabs.Tab>
                    <Tabs.Tab panelId="preview">Preview</Tabs.Tab>
                </Tabs.List>

                <Tabs.Panel id="editor">
                    <FormControl isRequired>
                        <StyledMultilineEditor>
                            <MultipleLineEditor
                                field={sdk.field}
                                locales={sdk.locales}
                            />
                        </StyledMultilineEditor>
                    </FormControl>
                </Tabs.Panel>

                <Tabs.Panel id="preview">
                    <CodeBlock
                        language={codeStyleValue}
                        code={codeInputValue}
                    />
                </Tabs.Panel>

            </Tabs>
        </Box>
    )
}

export default Field
